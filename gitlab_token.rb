# frozen_string_literal: true

# verifying https://gitlab.com/gitlab-org/gitlab/-/issues/299212, token will be revoked and cannot be used.
token = ""
